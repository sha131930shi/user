package com.sss.user.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sss.user.entity.User;
import com.sss.user.service.UserService;
import com.sss.user.service.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User createUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public Optional<User> getUser(Long userId) {
		Optional<User> dbUser = userRepository.findById(userId);
		
		if(dbUser.isEmpty())
			return Optional.empty();
		else
			return dbUser;
	}
}
