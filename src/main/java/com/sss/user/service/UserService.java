package com.sss.user.service;

import java.util.Optional;

import com.sss.user.entity.User;

public interface UserService {

	public User createUser(User user);

	public Optional<User> getUser(Long userId);

}
