package com.sss.user.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sss.user.entity.User;
import com.sss.user.service.UserService;

@RestController
public class UserController {

	Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping("/")
	public String home() {
		logger.error("Error");
		return "Welcome to SpringBoot!!!";
	}

	@PostMapping("/users")
	public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
		logger.debug("Entering createUser");
		User savedUser = userService.createUser(user);
		if(savedUser != null)
		return new ResponseEntity<User>(savedUser, HttpStatus.CREATED);
		else
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping("users/{userId}")
	public Optional<User> getUser(@PathVariable Long userId) {
		return userService.getUser(userId);
	}
}
